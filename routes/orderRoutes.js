const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController")


// Routes for create order (BUYER ONLY)
router.post("/createOrder/:productId", auth.verify, (req , res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		orderController.createOrder(userData, req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else{
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for retrieving User Order
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		orderController.getOrders(userData).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for updating User Order address or cancel if the status is pending (User Only)
router.patch("/myOrders/:orderId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		orderController.updateUserOrder(userData, req.params, req.body).then((resultFromController) => res.send (resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for retrieving all Orders based on status (ADMIN/SELLER)
router.post("/allOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		res.status(403).send("Unauthorized to view this")
	}else {
		orderController.allOrders(userData, req.body).then((resultFromController) => res.send (resultFromController))
	}
});


// Routes for updating status of specific Orders (ADMIN/SELLER)
router.patch("/updateOrder/:orderId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		res.status(403).send("Unauthorized to view this")
	}else {
		orderController.updateOrder(userData, req.params, req.body).then((resultFromController) => res.send (resultFromController))
	}
});



module.exports = router;