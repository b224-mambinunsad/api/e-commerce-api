const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController");


// Routes for adding products on the cart
router.post("/addToCart/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		cartController.addToCart(userData,req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for retrieving user cart
router.get("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		cartController.getCart(userData).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for updating qty of product in cart
router.patch("/update/:cartId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		cartController.updateCart(userData, req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for deleting product in cart
router.delete("/delete/:cartId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		cartController.deleteCart(userData, req.params).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});


// Routes for ordering all product in cart
router.post("/orderCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.role === "Buyer"){
		cartController.orderCart(userData, req.body).then((resultFromController) => res.send(resultFromController))
	}else {
		res.status(403).send("Unauthorized to view this")
	}
});





module.exports = router;