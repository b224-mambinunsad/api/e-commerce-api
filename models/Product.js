const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const productSchema = mongoose.Schema({
    shopName: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    sold: {
        type: Number,
        default: 0
    },
    isActive: {
        type: Boolean,
        default: true
    },
    reviews: [{
        userName: {
            type:String
        },
        rating: {
            type: Number
        },
        comment: {
            type: String
        }
    }]
},
{timestamps: true}
);

productSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Product", productSchema)