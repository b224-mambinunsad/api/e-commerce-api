const mongoose = require("mongoose");

const cartSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    shopName: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        productName: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            default: 1
        },
        price: {
            type: Number,
            required: true
        }
    }],
    totalAmount: {
        type: Number,
        required: true
    }
},
{timestamps: true}
);

module.exports = mongoose.model("Cart", cartSchema);