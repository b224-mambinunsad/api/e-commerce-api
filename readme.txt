FUNCTION THAT CAN DO ON THIS API

USER
	-user registraion with unqiueValidation on email
	-user login that will create Accesstoken to user
	-user can get hes profile details
	-owner can set role(Admin/Seller) on any user
	-owner can get all details of users
	-owner/seller can make thier own shopname only allow for the first time

	
PRODUCTS
	-admin/seller can add product with uniqueValidation on product name / the product that been added has a shopName of the admin or the seller shop
	-all can get active products
	-all can get single product
	-admin/seller can retrieve theor all products
	-all can get products by category
	-all can get top 5 hot product
	-add reviews on product (only allows if the user has a received order of the product)


ORDER
	-user can create a single order. ordered products stock/sold will auto update if the order is successfull
	-user can update address/contactNo of his order if the status is pending
	-admin/seller can get all order on thier shop based on status of the order
	-admin/seller can update the status of single order on thier shop


CART
	-user can add product to their cart
	-user can update product on their cart
	-user can remove product on their cart
	-user can order all products on their cart(products will auto update stock/sold if order is successfull)
