// Express Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");

const app = express();
const port = process.env.PORT || 4000;


// Mongoose Connection
mongoose.set("strictQuery", false);
mongoose.connect("mongodb+srv://Mambinunsad-224:Admin123@224-mambinunsad.uucqztz.mongodb.net/ecommerce-API-v2?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => console.log("Connected to MongoDB"))
.catch((error) => console.log("Connection Error"));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);



app.listen(port, () => console.log(`API is now running at ${port}`));