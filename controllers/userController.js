const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { update } = require("../models/User");


// Function for user registration
module.exports.userRegistration = async (reqBody) => {
    try {
        let newUser = new User({
            ...reqBody,
            password: bcrypt.hashSync(reqBody.password, 10)
        });
        const res = await newUser.save()
        return (res ? true : false)
    }catch(error){
        return (error.errors.email ? error : false)
    }
};


// Function for user login
module.exports.userLogin = async (reqBody) => {
    try {
        const res =  await User.findOne({email: reqBody.email})
        if(res === null){
            return false
        }else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, res.password)
            return (isPasswordCorrect ? {access: auth.createAccessToken(res)} : false)
        }
    }catch(error){
        return false
    }
};


// Function for getting details
module.exports.getDetails = async (userData) => {
	try {
		const details =  await User.findById(userData.id)
		if(details){
			details.password = "**********"
			return details
		}else{
			return false
		}
	}catch(error){
		return error
	}
};


// Function for setting user role (OWNER ONLY)
module.exports.setRole = async (userData, reqBody) => {
    try {
        const res = await User.findOne({email: reqBody.email})
        if(res === null){
            return false
        }else {
            let setRole = {
                role: reqBody.role
            }
            if(setRole.role === "Admin"){
                setRole = {
                    ...setRole,
                    shopName: userData.shopName
                }
            }
            await User.findByIdAndUpdate(res.id, setRole)
            return true
        }
    }catch(error){
        return false
    }
};


// Function for getting all user details (OWNER ONLY)
module.exports.getAllDetails = async () => {
	try {
		const res = await User.find()
		return (res.length === 0 ? false : res)
	}catch(error){
		return "Error" //false
	}
};


// Function for setting shop name(SELLER/OWNER ONLY)
module.exports.setShopName = async (userData, reqBody) => {
    try {
        if(userData.shopName !== "undefined" ){
            return "You already have shopname"
        }else{
            let update = {
                shopName: reqBody.shopName
            }
            const res =  await User.findOne({shopName: update.shopName})
            console.log(res)
            if(res){
                return false
            }else{
                await User.findByIdAndUpdate(userData.id, update)
                return true
            }
        }
    }catch(error){
        return error
    }
};


// Function for updating password
module.exports.updateDetails = async (userData, reqBody) => {
    try {
        const details =  await User.findById(userData.id)
        let update = {
                ...reqBody,
                password: bcrypt.hashSync(reqBody.password, 10)
            }
        if(details){
            await User.findByIdAndUpdate(userData.id, update)
            return true
        }else{
            return false
        }
        
    }catch(error){
        return error
    }
};