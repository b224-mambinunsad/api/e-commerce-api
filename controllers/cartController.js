const Cart = require("../models/Cart");
const Product = require("../models/Product");
const Order = require("../models/Order");



// Function for adding product to user cart
module.exports.addToCart = async (data, reqParams, reqBody) => {
        try {
            const product = await Product.findById(reqParams.productId)
            if(product == null){
                return  "Cannot find this product" //false
            }else if(product.stock - reqBody.quantity < 0){
                return  "Out of stock! please order based on qty of this product stock" //false
            }else{
                const userCarts = await Cart.find({userId: data.id})
                // let cart = new Cart ({
                //     userId: data.id,
                //     products: [{
                //         productId: product.id,
                //         productName: product.name,
                //         quantity: reqBody.quantity || 1,
                //         price: product.price
                //     }],
                //     totalAmount: product.price * (reqBody.quantity || 1)    
                // });
                let cart = new Cart ({
                    userId: data.id,
                    shopName: product.shopName,
                    products: [{
                        productId: reqParams.productId,
                        productName: product.name,
                        quantity: reqBody.quantity || 1,
                        price: product.price
                    }],
                    totalAmount: product.price * (reqBody.quantity || 1)
                })

                console.log(cart)

                //This will check user cart if empty it will order will be save as new cart
                let cartProductNames = [];
                if(userCarts.length === 0){
                    await cart.save()
                    return true //"Successfully added to your cart" //true
                }else{
                    for(i = 0; i < userCarts.length; i++){
                        cartProductNames.push(userCarts[i].products)
                    }
                };
                // This for loop check if the order product of user is arleady on his cart
                let isDuplicate;
                for(i = 0; i< cartProductNames.length; i++){
                    if(product.name === cartProductNames[i][0].productName){
                        isDuplicate = true
                        break;
                    }else{
                        isDuplicate = false
                    }
                };
                // This will add new cart if product is not already on cart of user
                if(isDuplicate === true){
                    return false
                }else{
                    await cart.save()
                    return true //"Successfully added to your cart" // true
                }
            }
    }catch(error){

        return error
    }
};


// Function for updating a users cart 
module.exports.updateCart = async (userData, reqParams, reqBody) => {
    try{
        const cart = await Cart.findById(reqParams.cartId)
        if(cart == null){
            return false //"Cannot find Cart Product" //false
        }else if(userData.id === cart.userId){
            let update = {
                products: [{
                    productId: cart.products[0].productId,
                    productName: cart.products[0].productName,
                    quantity: reqBody.quantity,
                    price: cart.products[0].price
                }],
                totalAmount: cart.products[0].price * reqBody.quantity

            };
            await Cart.findByIdAndUpdate(cart.id, update)
            return  true // "Cart has been updated" // true
        }else {
            return  false //"You are not the owner of this cart"// false
        }
        
    }catch(error){
        return  false // "Error" //false
    }
};


// Function for deleting a user cart
module.exports.deleteCart = async (userData, reqParams) => {
    try {
        const cart = await Cart.findById(reqParams.cartId)
        if(cart == null){
            return  false  //"Cannot find Cart Product" //false
        }else if(userData.id === cart.userId){
            await Cart.findByIdAndDelete(reqParams.cartId)
            return true //"Successfully removed this product from your cart" //true
        }else{
            return false //"You are not the owner of this cart"
        }
    }catch(error){
        return false //"Error" //false
    }
};


// Function for retrieving user cart
module.exports.getCart = async (data) => {
    try {
        const cart = await Cart.find({userId: data.id})
        if(cart.length === 0){
            return false //"No product found on your cart" //false
        }else{
            return cart //true
        }
    }catch(error){
        return "Error" //false
    }
};


// Function for ording the cart of the user
module.exports.orderCart = async (data,reqBody) => {
    try{
        const userCarts = await Cart.find({userId: data.id})
        if(userCarts.length === 0){
            return "You dont have products on your cart" //false
        }else{
            // let amount = 0;
            // this for loop will get the total amount of the ordered cart
            // for(i = 0; i < userCarts.length; i++){
            //      amount += userCarts[i].totalAmount
            // };
            // let order = new Order ({
            //     userId: data.id,
            //     userName: data.name,
            //     address: reqBody.address,
            //     products: [],
            //     totalAmount: amount,
            //     address: reqBody.address
            // });
            for(i = 0; i < userCarts.length; i++){
                
                let order = new Order ({
                    userId: data.id,
                    userName: data.name,
                    products: [{
                        shopName: userCarts[i].shopName,
                        productId: userCarts[i].products[0].productId,
                        productName: userCarts[i].products[0].productName,
                        quantity: userCarts[i].products[0].quantity,
                        price: userCarts[i].products[0].price 
                    }],
                    totalAmount: userCarts[i].totalAmount,
                    address: reqBody.address,
                    contactNo: reqBody.contactNo
                })

                let x = await Product.findById(userCarts[i].products[0].productId)
                let y = userCarts[i].products[0].quantity
                if(x){
                    let update = {
                        stock: x.stock - y,
                        sold: x.sold + y
                    }
                    if(update.stock <= 0){
                        update = {
                            ...update,
                            isActive: false
                        }
                    }
                    await Product.findByIdAndUpdate(x.id, update)
                }
                await order.save()
                await Cart.findByIdAndDelete(userCarts[i].id)

            };
            return true //"Cart has been successfully ordered" //true

            // This for loop is for pushing the products order to the var of order
            // and update the Product stocks
            // for(i = 0; i < userCarts.length; i++){
            //     // This will push the products orders into the var of order
            //     order.products.push({
            //         productId: userCarts[i].products[0].productId,
            //         productName: userCarts[i].products[0].productName,
            //         quantity: userCarts[i].products[0].quantity,
            //         price: userCarts[i].products[0].price,
            //     })

            //     // This is for updating the products stock 
            //     let x = await Product.findById(userCarts[i].products[0].productId)
            //     let y = userCarts[i].products[0].quantity
            //     if(x){
            //         let update = {
            //             stock: x.stock - y
            //         }
            //         if(update.stock <= 0){
            //             update = {
            //                 stock: 0,
            //                 isActive: false
            //             }
            //         }
            //         await Product.findByIdAndUpdate(x.id, update)
            //     }


            // };
            // This for loop will delete all cart of the user once order has been successfully process
            // for(i = 0; i < userCarts.length; i++){
            //     await Cart.findByIdAndDelete(userCarts[i].id)
            // };
            // return "Cart has been successfully ordered" //true
        }
    }catch(error){
        return false //error
    }
    
};





//OLD CART FUNCTIONS


// Routes for adding products on the cart
// module.exports.addToCart = async (userData, reqParams, reqBody) => {
//     try{
//         const userCarts = await Cart.find({userId: userData.id})
//         const orderedProduct = await Product.findById(reqParams.productId)
        // let cart = new Cart ({
        //     userId: userData.id,
        //     shopName: orderedProduct.shopName,
        //     products: [{
        //         productId: orderedProduct.id,
        //         productName: orderedProduct.name,
        //         quantity: reqBody.quantity || 1,
        //         price: orderedProduct.price
        //     }],
        //     totalAmount: orderedProduct.price * (reqBody.quantity || 1)
        // })

//         if(orderedProduct === null || orderedProduct.stock - reqBody.quantity < 0){
//            return false 
//         }else if(userCarts.length === 0){
//             await cart.save()
//             return true
//         }else{
//             let newCart = []
//             let amount = 0
//             userCarts.forEach((userCart) => {
//                 let isDupe = false
//                 userCart.products.forEach(product => {
//                     if(product.productId === orderedProduct.id){
//                         isDupe = true
//                     }
//                 })
//                 if(orderedProduct.shopName === userCart.shopName && isDupe === false ){
//                     newCart.push(userCart)
//                 }
//             })
//             if(newCart.length > 0){
//                 newCart[0].products.push({
//                     productId: orderedProduct.id,
//                     productName: orderedProduct.name,
//                     quantity: reqBody.quantity || 1,
//                     price: orderedProduct.price
//                 })
//                 newCart[0].products.forEach(product => {
//                     amount += (product.price * reqBody.quantity)
//                 })
//                 newCart[0].totalAmount = amount
//                 await Cart.findByIdAndUpdate(newCart[0].id, newCart[0])
//                 return true
//             }else {
//                 await cart.save()
//                 return true
//             }
//         }
//     }catch(error){
//         return (error._message ? "Duplicate product on your cart" : false)
//     }
// };


// // Function for retrieving user cart
// module.exports.getCart = async (userData) => {
//     try{
//         const userCart = await Cart.find({userId: userData.id})
//         return (userCart.length === 0 ? false : userCart)
//     }catch(error){
//         return error
//     }
// };


// // Function for updating qty of products in cart
// module.exports.updateCart =  async (userData, reqParams, reqBody) => {
//     try {
//         const userCart = await Cart.findById(reqParams.cartId)
//         if(userCart === null || userCart.userId !== userData.id){
//             return false
//         }else {
//             userCart.totalAmount = 0;
//             userCart.products.forEach((product => {
//                 if(product.productId === reqBody.productId){
//                     product.quantity = reqBody.quantity || 1
//                 }
//                 userCart.totalAmount += product.price * product.quantity
//             }))
//             await Cart.findByIdAndUpdate(userCart.id, userCart)
//             return true
//         }
//     }catch(error){
//         return error
//     }
// };


// // Function for deleting product in cart
// module.exports.deleteCart = async (userData, reqParams ,reqBody) => {
//     try {
//         const userCart =  await Cart.findById(reqParams.cartId)
//         if(userCart ===  null || userCart.userId !== userData.id){
//             return false
//         }else{
//             userCart.totalAmount = 0;
//             userCart.products.forEach((product) => {
//                 if(product.productId === reqBody.productId){
//                     const indexOfProdToRemove = userCart.products.indexOf(product)
//                     userCart.products.splice(indexOfProdToRemove, 1)
//                 }
//                 userCart.totalAmount += product.price * product.quantity
//             })
//             if(userCart.products.length === 0){
//                 await Cart.findByIdAndDelete(reqParams.cartId)
//                 return true
//             }else{
//                 await Cart.findByIdAndUpdate(userCart.id, userCart)
//                 return true
//             }
//         }
//     }catch(error){
//         return error
//     }
// };


// // Function for ordering all product in cart
// module.exports.orderCart = async (userData, reqBody) => {
//     try{
//         const userCarts = await Cart.find({userId: userData.id})
//         if(userCarts.length ===0){
//             return false
//         }else {
//             for(i = 0; i < userCarts.length; i++){
//                 let order = new Order ({
//                     userId: userData.id,
//                     userName: userData.name,
//                     products: [],
//                     totalAmount: userCarts[i].totalAmount,
//                     address: reqBody.address,
//                     contactNo: reqBody.contactNo
//                 })

//                 userCarts[i].products.forEach( async (product) => {
//                     order.products.push({
//                         shopName: userCarts[i].shopName,
//                         productId: product.productId,
//                         productName: product.productName,
//                         quantity: product.quantity,
//                         price: product.price
//                     })

//                     let cartProduct = await Product.findById(product.productId)
//                     let update = {
//                         stock: cartProduct.stock - product.quantity,
//                         sold: cartProduct.sold + product.quantity
//                     }
//                     if(update.stock <= 0){
//                         update ={
//                             ...update,
//                             isActive: false
//                         }
//                     }
//                     await Product.findByIdAndUpdate(cartProduct.id, update)
//                 })
//                 await order.save()
//                 await Cart.findByIdAndDelete(userCarts[i].id)
//             }
//             return true
//         }
//     }catch(error){
//         return error
//     }
// }